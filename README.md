## My Understanding and assumptions.

- All the customers should be taken care by Tellers
- Every Tellers get assigned once they finished assigned customers.
- All the Tellers should leave near by time once finished the last customer. Which means the Teller can work for many customers but everybody should finish all customers in same time. 


## Code changes
```  
 static void Calculation(CustomerList customers, TellerList tellers)
        {            
            int tellercount = 0; // assinging teller value 0 
            //Decending the customers by duration of appointment
            var newcustomersList = customers.Customer.OrderByDescending(x => x.duration).ToList();

            foreach (Customer customer in newcustomersList)
            {
                //First work assigned to Teller without calculating their work duration.
                //Who finishing the work first they will get assigned first.
                if (tellercount >= tellers.Teller.Count)
                {
                    //Finding which teller has minimum duration or who finished their work and ready to start other
                    var tellerAppointments = from appoint in appointmentList
                                             group appoint by appoint.teller into tellerGroup
                                             select new
                                             {
                                                 teller = tellerGroup.Key,
                                                 totalDuration = tellerGroup.Sum(x => x.duration),
                                             };
                    var min = tellerAppointments.OrderBy(i => i.totalDuration).FirstOrDefault();

                    var appointment = new Appointment(customer, min.teller);
                    appointmentList.Add(appointment);
                    //Here we know 150 Tellers So just printing the final working minutes for all Tellers
                    if (tellercount > 4850)
                    {
                        Console.WriteLine("Teller ID " + min.teller.id + "  takes , " + (min.totalDuration + appointment.duration) + " minutes!");
                    }
                }
                else
                {
                    var appointment = new Appointment(customer, tellers.Teller[tellercount]);
                    appointmentList.Add(appointment);
                    //Console.WriteLine("Teller " + tellers.Teller[tellercount].id + "  " + appointment.duration + " minutes!");
                }

                tellercount++;
            }
         }

```


## Output

```
Teller ID 9001238546  takes , 562 minutes!
Teller ID 9001238547  takes , 562 minutes!
Teller ID 9001238548  takes , 562 minutes!
Teller ID 9001238549  takes , 562 minutes!
Teller ID 9001238550  takes , 562 minutes!
Teller ID 9001238551  takes , 562 minutes!
Teller ID 9001238552  takes , 562 minutes!
Teller ID 9001238553  takes , 562 minutes!
Teller ID 9001238554  takes , 562 minutes!
Teller ID 9001238555  takes , 562 minutes!
Teller ID 9001238556  takes , 562 minutes!
Teller ID 9001238557  takes , 562 minutes!
Teller ID 9001238571  takes , 561 minutes!
Teller ID 9001238572  takes , 561 minutes!
Teller ID 9001238503  takes , 564 minutes!
Teller ID 9001238504  takes , 564 minutes!
Teller ID 9001238505  takes , 564 minutes!
Teller ID 9001238506  takes , 564 minutes!
Teller ID 9001238528  takes , 565 minutes!
Teller ID 9001238529  takes , 565 minutes!
Teller ID 9001238530  takes , 565 minutes!
Teller ID 9001238531  takes , 565 minutes!
Teller ID 9001238532  takes , 565 minutes!
Teller ID 9001238533  takes , 565 minutes!
Teller ID 9001238534  takes , 565 minutes!
Teller ID 9001238585  takes , 565 minutes!
Teller ID 9001238586  takes , 565 minutes!
Teller ID 9001238587  takes , 565 minutes!
Teller ID 9001238588  takes , 565 minutes!
Teller ID 9001238589  takes , 565 minutes!
Teller ID 9001238590  takes , 565 minutes!
Teller ID 9001238591  takes , 565 minutes!
Teller ID 9001238592  takes , 565 minutes!
Teller ID 9001238593  takes , 565 minutes!
Teller ID 9001238558  takes , 564 minutes!
Teller ID 9001238559  takes , 564 minutes!
Teller ID 9001238560  takes , 564 minutes!
Teller ID 9001238561  takes , 564 minutes!
Teller ID 9001238562  takes , 564 minutes!
Teller ID 9001238563  takes , 564 minutes!
Teller ID 9001238564  takes , 564 minutes!
Teller ID 9001238565  takes , 564 minutes!
Teller ID 9001238566  takes , 564 minutes!
Teller ID 9001238567  takes , 564 minutes!
Teller ID 9001238568  takes , 564 minutes!
Teller ID 9001238569  takes , 564 minutes!
Teller ID 9001238570  takes , 564 minutes!
Teller ID 9001238475  takes , 568 minutes!
Teller ID 9001238476  takes , 568 minutes!
Teller ID 9001238477  takes , 568 minutes!
Teller ID 9001238478  takes , 568 minutes!
Teller ID 9001238479  takes , 568 minutes!
Teller ID 9001238480  takes , 568 minutes!
Teller ID 9001238481  takes , 568 minutes!
Teller ID 9001238482  takes , 568 minutes!
Teller ID 9001238483  takes , 568 minutes!
Teller ID 9001238484  takes , 568 minutes!
Teller ID 9001238497  takes , 568 minutes!
Teller ID 9001238498  takes , 568 minutes!
Teller ID 9001238499  takes , 568 minutes!
Teller ID 9001238500  takes , 568 minutes!
Teller ID 9001238501  takes , 568 minutes!
Teller ID 9001238502  takes , 568 minutes!
Teller ID 9001238507  takes , 568 minutes!
Teller ID 9001238508  takes , 568 minutes!
Teller ID 9001238509  takes , 568 minutes!
Teller ID 9001238510  takes , 568 minutes!
Teller ID 9001238511  takes , 568 minutes!
Teller ID 9001238512  takes , 569 minutes!
Teller ID 9001238513  takes , 569 minutes!
Teller ID 9001238514  takes , 569 minutes!
Teller ID 9001238515  takes , 569 minutes!
Teller ID 9001238516  takes , 569 minutes!
Teller ID 9001238517  takes , 569 minutes!
Teller ID 9001238518  takes , 569 minutes!
Teller ID 9001238519  takes , 569 minutes!
Teller ID 9001238520  takes , 569 minutes!
Teller ID 9001238521  takes , 569 minutes!
Teller ID 9001238522  takes , 569 minutes!
Teller ID 9001238523  takes , 569 minutes!
Teller ID 9001238524  takes , 569 minutes!
Teller ID 9001238525  takes , 569 minutes!
Teller ID 9001238526  takes , 569 minutes!
Teller ID 9001238527  takes , 569 minutes!
Teller ID 9001238573  takes , 567 minutes!
Teller ID 9001238574  takes , 567 minutes!
Teller ID 9001238575  takes , 567 minutes!
Teller ID 9001238576  takes , 567 minutes!
Teller ID 9001238577  takes , 567 minutes!
Teller ID 9001238578  takes , 567 minutes!
Teller ID 9001238579  takes , 567 minutes!
Teller ID 9001238580  takes , 567 minutes!
Teller ID 9001238581  takes , 567 minutes!
Teller ID 9001238582  takes , 567 minutes!
Teller ID 9001238583  takes , 567 minutes!
Teller ID 9001238584  takes , 567 minutes!
Teller ID 9001238456  takes , 570 minutes!
Teller ID 9001238457  takes , 570 minutes!
Teller ID 9001238458  takes , 570 minutes!
Teller ID 9001238459  takes , 570 minutes!
Teller ID 9001238460  takes , 570 minutes!
Teller ID 9001238461  takes , 570 minutes!
Teller ID 9001238462  takes , 570 minutes!
Teller ID 9001238463  takes , 570 minutes!
Teller ID 9001238464  takes , 570 minutes!
Teller ID 9001238465  takes , 570 minutes!
Teller ID 9001238466  takes , 570 minutes!
Teller ID 9001238467  takes , 570 minutes!
Teller ID 9001238468  takes , 570 minutes!
Teller ID 9001238469  takes , 570 minutes!
Teller ID 9001238470  takes , 570 minutes!
Teller ID 9001238471  takes , 570 minutes!
Teller ID 9001238472  takes , 570 minutes!
Teller ID 9001238473  takes , 570 minutes!
Teller ID 9001238474  takes , 570 minutes!
Teller ID 9001238594  takes , 570 minutes!
Teller ID 9001238595  takes , 570 minutes!
Teller ID 9001238596  takes , 570 minutes!
Teller ID 9001238597  takes , 570 minutes!
Teller ID 9001238598  takes , 570 minutes!
Teller ID 9001238599  takes , 570 minutes!
Teller ID 9001238600  takes , 570 minutes!
Teller ID 9001238601  takes , 570 minutes!
Teller ID 9001238602  takes , 570 minutes!
Teller ID 9001238603  takes , 570 minutes!
Teller ID 9001238604  takes , 570 minutes!
Teller ID 9001238571  takes , 569 minutes!
Teller ID 9001238572  takes , 569 minutes!
Teller ID 9001238485  takes , 572 minutes!
Teller ID 9001238486  takes , 572 minutes!
Teller ID 9001238487  takes , 572 minutes!
Teller ID 9001238488  takes , 572 minutes!
Teller ID 9001238489  takes , 572 minutes!
Teller ID 9001238490  takes , 572 minutes!
Teller ID 9001238491  takes , 572 minutes!
Teller ID 9001238492  takes , 572 minutes!
Teller ID 9001238493  takes , 572 minutes!
Teller ID 9001238494  takes , 572 minutes!
Teller ID 9001238495  takes , 572 minutes!
Teller ID 9001238496  takes , 572 minutes!
Teller ID 9001238535  takes , 571 minutes!
Teller ID 9001238536  takes , 571 minutes!
Teller ID 9001238537  takes , 571 minutes!
Teller ID 9001238538  takes , 571 minutes!
Teller ID 9001238539  takes , 571 minutes!
Teller ID 9001238540  takes , 571 minutes!
Teller ID 9001238541  takes , 571 minutes!
Teller ID 9001238542  takes , 571 minutes!
Teller ID 9001238543  takes , 571 minutes!
Teller 9001238496 will work for 572 minutes!
