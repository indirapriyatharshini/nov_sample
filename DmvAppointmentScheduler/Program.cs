﻿using System;
using System.IO;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Collections;

namespace DmvAppointmentScheduler
{
    class Program
    {
        public static Random random = new Random();
        public static List<Appointment> appointmentList = new List<Appointment>();
        static void Main(string[] args)
        {
            CustomerList customers = ReadCustomerData();
            TellerList tellers = ReadTellerData();
            Calculation(customers, tellers);
            OutputTotalLengthToConsole();
            Console.ReadLine();

        }
        private static CustomerList ReadCustomerData()
        {
            string fileName = "CustomerData.json";
            string path = Path.Combine(Environment.CurrentDirectory, @"InputData\", fileName);
            string jsonString = File.ReadAllText(path);
            CustomerList customerData = JsonConvert.DeserializeObject<CustomerList>(jsonString);
            return customerData;

        }
        private static TellerList ReadTellerData()
        {
            string fileName = "TellerData.json";
            string path = Path.Combine(Environment.CurrentDirectory, @"InputData\", fileName);
            string jsonString = File.ReadAllText(path);
            TellerList tellerData = JsonConvert.DeserializeObject<TellerList>(jsonString);
            return tellerData;

        }
        static void Calculation(CustomerList customers, TellerList tellers)
        {            
            int tellercount = 0; // assinging teller value 0 

            //Decending the customers by duration of appointment
            var newcustomersList = customers.Customer.OrderByDescending(x => x.duration).ToList();

            foreach (Customer customer in newcustomersList)
            {
                //First work assigned to Teller without calculating their work duration.
                //Who finishing the work first they will get assigned first.
                if (tellercount >= tellers.Teller.Count)
                {
                    //Finding which teller has minimum duration or who finished their work and ready to start other
                    var tellerAppointments = from appoint in appointmentList
                                             group appoint by appoint.teller into tellerGroup
                                             select new
                                             {
                                                 teller = tellerGroup.Key,
                                                 totalDuration = tellerGroup.Sum(x => x.duration),
                                             };
                    var min = tellerAppointments.OrderBy(i => i.totalDuration).FirstOrDefault();

                    var appointment = new Appointment(customer, min.teller);
                    appointmentList.Add(appointment);
                    //Here we know 150 Tellers So just printing the final working minutes for all Tellers
                    if (tellercount > 4850)
                    {
                        Console.WriteLine("Teller ID " + min.teller.id + "  takes , " + (min.totalDuration + appointment.duration) + " minutes!");
                    }
                }
                else
                {
                    var appointment = new Appointment(customer, tellers.Teller[tellercount]);
                    appointmentList.Add(appointment);
                    //Console.WriteLine("Teller " + tellers.Teller[tellercount].id + "  " + appointment.duration + " minutes!");
                }

                tellercount++;
            }
         }
        static void OutputTotalLengthToConsole()
        {
            var tellerAppointments =
                from appointment in appointmentList
                group appointment by appointment.teller into tellerGroup
                select new
                {
                    teller = tellerGroup.Key,
                    totalDuration = tellerGroup.Sum(x => x.duration),
                };
            var max = tellerAppointments.OrderBy(i => i.totalDuration).LastOrDefault ();
            Console.WriteLine("Teller " + max.teller.id + " will work for " + max.totalDuration + " minutes!");
        }

    }
}
